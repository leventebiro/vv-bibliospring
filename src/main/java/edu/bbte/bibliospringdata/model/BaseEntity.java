package edu.bbte.bibliospringdata.model;


import jakarta.persistence.*;

@MappedSuperclass
public abstract class BaseEntity extends Abstractmodel {
    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return super.toString() + id + " ";
    }
}
