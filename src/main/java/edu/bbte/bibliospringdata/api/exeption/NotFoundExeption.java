package edu.bbte.bibliospringdata.api.exeption;

public class NotFoundExeption extends RuntimeException {
    private final Class type;
    private final Long id;

    public NotFoundExeption(Class type, Long id) {
        super();
        this.type = type;
        this.id = id;
    }

    public Class getType() {
        return type;
    }

    public Long getId() {
        return id;
    }
}
