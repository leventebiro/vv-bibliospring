package edu.bbte.bibliospringdata.api;

import edu.bbte.bibliospringdata.api.exeption.NotFoundExeption;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@ControllerAdvice
public class ApiExeptionHandler {

    @ExceptionHandler({NotFoundExeption.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public String handlerNotFoundExeption(NotFoundExeption e) {
        return e.getType().getName() + "Entity not found with id:" + e.getId();
    }
}
