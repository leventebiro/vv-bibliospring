package edu.bbte.bibliospringdata.util;

import org.springframework.stereotype.Component;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Component
public class PasswordEncrypter {
    public String generatedHashedPassword(String password, String salt) {
        try {
            byte[] input = (password + salt).getBytes();
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.reset();
            messageDigest.update(input);

            byte[] output = messageDigest.digest();
            StringBuilder sb = new StringBuilder();
            for (byte byteHashing : output) {
                sb.append(String.format("%02x", byteHashing));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new SecurityException("Algorithm not found", e);
        }
    }
}
