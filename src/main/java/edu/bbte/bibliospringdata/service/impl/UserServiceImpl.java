package edu.bbte.bibliospringdata.service.impl;

import edu.bbte.bibliospringdata.model.User;
import edu.bbte.bibliospringdata.service.impl.repository.UserRepository;
import edu.bbte.bibliospringdata.service.UserService;
import edu.bbte.bibliospringdata.util.PasswordEncrypter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    PasswordEncrypter passwordEncrypter;

    @Autowired
    UserRepository userRepository;

    @Override
    public User create(User user) {
        user.setPassword(passwordEncrypter.generatedHashedPassword(user.getPassword(), user.getUid()));
        return userRepository.saveAndFlush(user);
    }

    @Override
    public User update(User user) {
        return userRepository.saveAndFlush(user);
    }

    @Override
    public boolean login(User user) {
        User dbUser = userRepository.findByUserName(user.getUserName());
        if (dbUser != null) {
            user.setPassword(passwordEncrypter.generatedHashedPassword(user.getPassword(), dbUser.getUid()));
            return dbUser.getPassword().equals(user.getPassword());
        }
        return false;
    }

    @Override
    public boolean deleteById(Long id) {
        if (userRepository.existsById(id)) {
            userRepository.deleteById(id);
        } else {
            return false;
        }
        return true;
    }

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @Override
    public User getById(Long id) {
        return userRepository.findById(id).orElse(null);
    }
}
