package edu.bbte.bibliospringdata.assembler;

import edu.bbte.bibliospringdata.dto.incoming.BookInDTO;
import edu.bbte.bibliospringdata.dto.outgoing.BookOutDTO;
import edu.bbte.bibliospringdata.model.Author;
import edu.bbte.bibliospringdata.model.Book;
import edu.bbte.bibliospringdata.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BookAssembler {

    @Autowired
    private AuthorService authorService;
    @Autowired
    private AuthorAssembler authorAssembler;

    public Book bookINDtoToModel(BookInDTO bookInDTO) {
        Book book = new Book();
        book.setTitle(bookInDTO.getTitle());
        book.setPublishedYear(bookInDTO.getPublishedYear());
        book.setGenre(bookInDTO.getGenre());
        Author author = authorService.getById(book.getAuthorId());
        book.setAuthor(author);
        return book;
    }

    public BookOutDTO modelToBookOutDto(Book book) {
        BookOutDTO bookOutDTO = new BookOutDTO();
        bookOutDTO.setUid(book.getUid());
        bookOutDTO.setId(book.getId());
        bookOutDTO.setTitle(book.getTitle());
        bookOutDTO.setGenre(book.getGenre());
        bookOutDTO.setPublishedYear(book.getPublishedYear());
        Author author = authorService.getById(book.getAuthor().getId());
        bookOutDTO.setAuthor(author);
        return bookOutDTO;
    }
}
