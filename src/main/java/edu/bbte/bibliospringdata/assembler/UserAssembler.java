package edu.bbte.bibliospringdata.assembler;

import edu.bbte.bibliospringdata.dto.incoming.UserInDTO;
import edu.bbte.bibliospringdata.dto.outgoing.UserOutDTO;
import edu.bbte.bibliospringdata.model.User;
import org.springframework.stereotype.Component;

@Component
public class UserAssembler {
    public User userInDtoToModel(UserInDTO userInDTO) {
        User user = new User();
        user.setUserName(userInDTO.getUser());
        user.setPassword(userInDTO.getPassword());
        return user;
    }

    public UserOutDTO modelToUserOutDto(User user) {
        UserOutDTO userOutDTO = new UserOutDTO();
        userOutDTO.setUid(user.getUid());
        userOutDTO.setId(user.getId());
        userOutDTO.setUser(user.getUserName());
        return userOutDTO;
    }
}
