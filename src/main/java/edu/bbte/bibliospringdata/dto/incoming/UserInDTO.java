package edu.bbte.bibliospringdata.dto.incoming;

public class UserInDTO {
    private String username;
    private String password;


    public String getUser() {
        return username;
    }

    public void setUser(String input) {
        this.username = input;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
