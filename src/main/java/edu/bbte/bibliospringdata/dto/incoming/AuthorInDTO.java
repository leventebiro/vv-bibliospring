package edu.bbte.bibliospringdata.dto.incoming;

public class AuthorInDTO {
    private final String firstName;
    private final String lastName;
    private Integer dateOfBirth;

    public AuthorInDTO(String authorFirstName, String authorLastName) {
        this.firstName = authorFirstName;
        this.lastName = authorLastName;
    }

    public Integer getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Integer dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getAuthorGivenName() { // Renamed for consistency
        return firstName;
    }

    public String getAuthorLastName() { // Renamed for consistency
        return lastName;
    }
}
