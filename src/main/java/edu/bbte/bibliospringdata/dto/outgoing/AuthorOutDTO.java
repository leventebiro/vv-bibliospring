package edu.bbte.bibliospringdata.dto.outgoing;

public class AuthorOutDTO {
    private String uid;
    private Long id;
    private String firstname;
    private String lastname;
    private Integer dateOfBirth;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAuthorFirstName() {
        return firstname;
    }

    public void setAuthorFirstName(String input) {
        this.firstname = input;
    }

    public String getAuthorLastName() {
        return lastname;
    }

    public void setAuthorLastName(String input) {
        this.lastname = input;
    }

    public Integer getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Integer dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
}
