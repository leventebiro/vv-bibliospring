package edu.bbte.bibliospringdata.service;

import edu.bbte.bibliospringdata.model.Author;
import edu.bbte.bibliospringdata.model.Book;
import edu.bbte.bibliospringdata.service.impl.repository.BookRepository;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@SpringBootTest
@Transactional
class BookServiceTest {

    @MockBean
    private BookRepository bookRepository;

    @Autowired
    private BookService bookService;

    private Book existingBook;

    @BeforeEach
    void initializing() {
        Author sampleAuthor = new Author();
        sampleAuthor.setId(1L);
        sampleAuthor.setFirstName("John");
        sampleAuthor.setLastName("Doe");
        sampleAuthor.setDateOfBirth(1980);

        existingBook = new Book();
        existingBook.setId(1L);
        existingBook.setTitle("Sample Book");
        existingBook.setAuthor(sampleAuthor);
        existingBook.setPublishedYear(2022);
        existingBook.setGenre("Fiction");
        existingBook.setId(1L);

    }

    @Test
    void testCreateBook() {
        //given
        given(bookRepository.saveAndFlush(any(Book.class))).willReturn(existingBook);

        //when
        Book createdBook = bookService.create(existingBook);

        //then
        assertNotNull(createdBook, "Book added succesfully");
    }

    @Test
    void testUpdateBook() {
        //given
        given(bookRepository.saveAndFlush(any(Book.class))).willReturn(existingBook);

        //when
        Book updatedBook = bookService.update(existingBook);

        //then
        assertNotNull(updatedBook, "Exepected a not null response");
    }
}
