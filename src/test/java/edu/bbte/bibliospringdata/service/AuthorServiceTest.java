package edu.bbte.bibliospringdata.service;

import edu.bbte.bibliospringdata.model.Author;
import edu.bbte.bibliospringdata.service.impl.repository.AuthorRepository;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@SpringBootTest
@Transactional
class AuthorServiceTest {

    @Autowired
    private AuthorService authorService;

    @MockBean
    private AuthorRepository authorRepository;

    private Author existingAuthor;

    @BeforeEach
    void setUp() {
        existingAuthor = new Author();
        existingAuthor.setId(1L);
        existingAuthor.setFirstName("Mark");
        existingAuthor.setLastName("Twain");
        existingAuthor.setDateOfBirth(1835);

        Author nonExistingAuthor = new Author();
        nonExistingAuthor.setId(2L);
        nonExistingAuthor.setFirstName("Non");
        nonExistingAuthor.setLastName("Existing");
        nonExistingAuthor.setDateOfBirth(2000);
    }

    @Test
    void testCreateAuthor() {
        //given
        given(authorRepository.saveAndFlush(any(Author.class))).willReturn(existingAuthor);

        //when
        Author createdAuthor = authorService.create(existingAuthor);

        //then
        assertNotNull(createdAuthor, "Author should not be null");
    }

    @Test
    void testUpdateAuthor() {
        //given
        given(authorRepository.saveAndFlush(any(Author.class))).willReturn(existingAuthor);

        //when
        Author updatedAuthor = authorService.update(existingAuthor);

        //then
        assertNotNull(updatedAuthor, "Author should not be null");
    }

    @Test
    void testDeleteExistingAuthorById() {
        //given
        given(authorRepository.existsById(1L)).willReturn(true);

        //when
        boolean result = authorService.deleteById(1L);

        //then
        assertTrue(result, "Expected a true value");
    }

    @Test
    void testDeleteNonexistentAuthorById() {
        //given
        given(authorRepository.existsById(2L)).willReturn(false);

        //when
        boolean result = authorService.deleteById(2L);

        //then
        assertFalse(result, "Expected a false value");
    }

    @Test
    void testGetAllAuthors() {
        //given
        given(authorRepository.findAll()).willReturn(Arrays.asList(existingAuthor, new Author()));

        //when
        List<Author> allAuthors = authorService.getAll();

        //then
        assertEquals(2, allAuthors.size(), "Expected a list of authors");
    }

    @Test
    void testGetAuthorById() {
        //given
        given(authorRepository.findById(1L)).willReturn(Optional.of(existingAuthor));

        //when
        Author authorSample = authorService.getById(1L);

        //then
        assertNotNull(authorSample, "Expected the calling is not null");
    }

    @Test
    void testGetAuthorByIdNotFound() {
        //given
        given(authorRepository.findById(2L)).willReturn(Optional.empty());

        //when
        Author foundAuthor = authorService.getById(2L);

        //then
        assertNull(foundAuthor, "Expected the author not found");
    }
}
