package edu.bbte.bibliospringdata.service;

import edu.bbte.bibliospringdata.model.User;
import edu.bbte.bibliospringdata.service.impl.repository.UserRepository;
import edu.bbte.bibliospringdata.util.PasswordEncrypter;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional
class UserServiceTest {
    @Autowired
    private UserService userService;
    @MockBean
    private UserRepository userRepository;
    @MockBean
    private PasswordEncrypter passwordEncrypter;

    private User dbUser;
    private User loggedInUser;
    private User wrongPassworsUser;
    private User notExistUser;


    @BeforeEach
    void setupUsers() {
        dbUser = new User();
        dbUser.setUserName("Ubulka");
        dbUser.setPassword("ubulka");

        loggedInUser = new User();
        loggedInUser.setUserName("Ubulka");
        loggedInUser.setPassword("ubulka");

        wrongPassworsUser = new User();
        wrongPassworsUser.setUserName("Ubulka");
        wrongPassworsUser.setPassword("ubul");

        notExistUser = new User();
        notExistUser.setUserName("Jozsi");
        notExistUser.setPassword("ubul");
    }

    @Test
    void testLoginWithCorrectPassword() {
        //given
        given(userRepository.findByUserName("Ubulka")).willReturn(dbUser);
        given(passwordEncrypter.generatedHashedPassword(eq("ubulka"), anyString())).willReturn("ubulka");

        //when
        boolean exitingUserGoodPassword = userService.login(loggedInUser);

        //then
        assertTrue(exitingUserGoodPassword, "Expected a succesfull login");
    }

    @Test
    void testLoginWithWrongPassword() {
        //given
        given(userRepository.findByUserName("Ubulka")).willReturn(dbUser);
        given(passwordEncrypter.generatedHashedPassword(eq("ubulka"), anyString())).willReturn("ubul"); // rossz jelszó

        //when
        boolean exitingUserWrongPassword = userService.login(wrongPassworsUser);

        //then
        assertFalse(exitingUserWrongPassword, "Expected a login failed process");
    }

    @Test
    void testLoginWithNonexistentUser() {
        //given
        given(userRepository.findByUserName("Jozsi")).willReturn(null);

        //when
        boolean wrongUser = userService.login(notExistUser);

        //then
        assertFalse(wrongUser, "Expected an autorized user");
    }

    @Test
    void testLoginHandlesNullFromRepository() {
        //given
        given(userRepository.findByUserName("Ubulka")).willReturn(null);

        //when
        boolean result = userService.login(loggedInUser);

        //then
        assertFalse(result, "Expected a false value");
    }
}
