package edu.bbte.bibliospringdata.api;

import edu.bbte.bibliospringdata.assembler.UserAssembler;
import edu.bbte.bibliospringdata.dto.incoming.UserInDTO;
import edu.bbte.bibliospringdata.dto.outgoing.UserOutDTO;
import edu.bbte.bibliospringdata.model.User;
import edu.bbte.bibliospringdata.service.UserService;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@SpringBootTest
@Transactional
class UserControllerTest {

    @InjectMocks
    private UserController userController;

    @MockBean
    private UserService userService;

    @MockBean
    private UserAssembler userAssembler;

    private User sampleUser;
    private UserInDTO sampleUserInDTO;
    private UserOutDTO sampleUserOutDTO;

    @BeforeEach
    void setUpTestUser() {
        MockitoAnnotations.openMocks(this);

        sampleUser = new User();
        sampleUser.setId(1L);
        sampleUser.setUserName("john_doe");
        sampleUser.setPassword("password");

        sampleUserInDTO = new UserInDTO();
        sampleUserInDTO.setUser("john_doe");
        sampleUserInDTO.setPassword("password");

        sampleUserOutDTO = new UserOutDTO();
        sampleUserOutDTO.setId(1L);
        sampleUserOutDTO.setUser("john_doe");
    }

    @Test
    void testGetAll() {
        given(userService.getAll()).willReturn(Arrays.asList(sampleUser));
        given(userAssembler.modelToUserOutDto(sampleUser)).willReturn(sampleUserOutDTO);

        List<UserOutDTO> result = userController.getAll();

        assertEquals(1, result.size(), "Return sample user");
        verify(userAssembler).modelToUserOutDto(any(User.class));
    }

    @Test
    void testGetById() {
        given(userService.getById(1L)).willReturn(sampleUser);
        given(userAssembler.modelToUserOutDto(sampleUser)).willReturn(sampleUserOutDTO);

        UserOutDTO result = userController.getById(1L);

        assertEquals(1L, result.getId(), "Expected sample user id");
        verify(userAssembler).modelToUserOutDto(any(User.class));
    }

    @Test
    void testRegisterUser() {
        given(userService.create(any(User.class))).willReturn(sampleUser);
        given(userAssembler.modelToUserOutDto(sampleUser)).willReturn(sampleUserOutDTO);
        given(userAssembler.userInDtoToModel(any(UserInDTO.class))).willReturn(sampleUser);

        ResponseEntity<UserOutDTO> result = userController.registerUser(sampleUserInDTO);

        assertEquals(1L, result.getBody().getId(), "Waiting sample user id");
        verify(userAssembler).modelToUserOutDto(any(User.class));
        verify(userAssembler).userInDtoToModel(any(UserInDTO.class));

    }

}
