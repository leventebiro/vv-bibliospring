package edu.bbte.bibliospringdata.api;

import edu.bbte.bibliospringdata.assembler.BookAssembler;
import edu.bbte.bibliospringdata.dto.incoming.BookInDTO;
import edu.bbte.bibliospringdata.dto.outgoing.BookOutDTO;
import edu.bbte.bibliospringdata.model.Author;
import edu.bbte.bibliospringdata.model.Book;
import edu.bbte.bibliospringdata.service.AuthorService;
import edu.bbte.bibliospringdata.service.BookService;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;


import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@SpringBootTest
@Transactional
class BookControllerTest {

    @Mock
    private BookService bookService;

    @Mock
    private AuthorService authorService;

    @Mock
    private BookAssembler bookAssembler;

    @InjectMocks
    private BookController bookController;

    private Book sampleBook;
    private Author sampleAuthor;

    @BeforeEach
    void setUpBookforTesting() {
        MockitoAnnotations.openMocks(this);
        sampleAuthor = new Author();
        sampleAuthor.setId(1L);
        sampleAuthor.setFirstName("John");
        sampleAuthor.setLastName("Doe");
        sampleAuthor.setDateOfBirth(1980);

        sampleBook = new Book();
        sampleBook.setId(1L);
        sampleBook.setTitle("Sample Book");
        sampleBook.setAuthor(sampleAuthor);
        sampleBook.setPublishedYear(2022);
        sampleBook.setGenre("Fiction");
        sampleBook.setAuthorID(1L);
    }

    @Test
    void testGetBooksByTitleContains() {

        given(bookService.findByTitleContains("Sample")).willReturn(Arrays.asList(sampleBook));
        List<BookOutDTO> result2 = bookController.getBooksByTitleContains("Sample");
        assertEquals(1, result2.size(), "Ony one item");
        verify(bookAssembler).modelToBookOutDto(any(Book.class));
    }

    @Test
    void testAddBook() {

        given(authorService.create(any(Author.class))).willReturn(sampleAuthor);
        given(bookService.create(any(Book.class))).willReturn(sampleBook);
        given(bookAssembler.modelToBookOutDto(sampleBook)).willReturn(createSampleBookOutDTO());
        ResponseEntity<BookOutDTO> result1 = bookController.addBook(createSampleBookInDTO());
        assertEquals(1L, result1.getBody().getId(), "Book insertation");
        verify(bookAssembler).modelToBookOutDto(sampleBook);

    }

    private BookInDTO createSampleBookInDTO() {
        BookInDTO bookInDTO = new BookInDTO();
        bookInDTO.setTitle("Sample Book");
        bookInDTO.setPublishedYear(2022);
        bookInDTO.setGenre("Fiction");
        bookInDTO.setAuthor(sampleAuthor);
        return bookInDTO;
    }

    private BookOutDTO createSampleBookOutDTO() {
        BookOutDTO bookOutDTO = new BookOutDTO();
        bookOutDTO.setId(1L);
        bookOutDTO.setTitle("Sample Book");
        bookOutDTO.setAuthor(sampleAuthor);
        bookOutDTO.setPublishedYear(2022);
        bookOutDTO.setGenre("Fiction");
        return bookOutDTO;
    }
}
